<?php

// OBJECT
//Using procedural approach

$buildingObject = (object)[
'name' => 'Caswynn Building',
'floors' => 8,
'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];


//Objects from Classes

class Building{

	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address){
		$this ->name = $name;
		$this ->floors = $floors;
		$this -> address = $address;
	}


		public function getName(){
		return $this->name;
	}

	public function setName($name){
		//$this->name = $name;
		if(strlen($name) !==0 ){
			$this->name = $name;
		}
	}

	
}

	
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines' );


//INHERITANCE & POLYMORPHISM

class Condominium extends Building{
	
	// 	public function getName(){
	// 	return $this->name;
	// }

	// public function setName($name){
	// 	//$this->name = $name;
	// 	if(strlen($name) !==0 ){
	// 		$this->name = $name;
	// 	}
	// }

}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
















